# -*- coding: utf-8 -*-
import logging
import json
from grab import Grab
from grab.spider import Spider, Task
from grab.document import Document
from conf import sites
import os
from io import StringIO
import lxml
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class BetSpider(Spider):
    outputs = {}
    counter = 0
    ajax_counter = 0

    def prepare(self):
        for site in sites:
            self.outputs[site.outfile] = open(site.outfile, 'wb')
            self.outputs[site.outfile].write(b'[')
        # self.outputs[self.ajax_site.outfile] = open(self.ajax_site.outfile, 'wb')
        # self.outputs[self.ajax_site.outfile].write(b'[')
        return super(BetSpider, self).prepare()

    def shutdown(self):
        for site in sites:
            self.outputs[site.outfile].seek(-2, os.SEEK_END)
            self.outputs[site.outfile].write(b']')
            self.outputs[site.outfile].close()

        # self.outputs[self.ajax_site.outfile].seek(-2, os.SEEK_END)
        # self.outputs[self.ajax_site.outfile].write(b']')
        # self.outputs[self.ajax_site.outfile].close()
        return super(BetSpider, self).shutdown()

    def dump_to_file(self, data, output):
        data = json.dumps(data, ensure_ascii=False)
        output.write(data.encode('utf8'))
        output.write(b',\n')

    def task_generator(self):
        g = Grab()
        for site in sites:
            s = site()
            yield Task('main', url=s.allcollections, site=s, use_proxylist=False, network_try_count=3, task_try_count=4,)

    def task_main(self, grab, task):
        logger.info('main {}'.format(task.url))
        self.counter += 1
        site = task.site
        doc = grab.doc
        resp_list = doc.select(site.links).node_list()
        for r_url in sorted(resp_list):
            url = r_url.replace('\n', '')
            if not '_' in url:
                continue
            yield Task('detail', url=''.join([site.root_url, url]), site=site, use_proxylist=False)  #
        next = doc.select(site.next_page).node(default=None)
        logger.info('---- MAIN COUNT {} ----'.format(self.counter))
        if next:
            yield Task('main', url=next, site=site, use_proxylist=False)  # network_try_count=3, task_try_count=4,

    def task_detail(self, grab, task):
        resp = grab.response
        if not resp.code == 200:
            return
        self.ajax_counter += 1
        logger.info('load info {}'.format(task.url))
        data = {'url': task.url}
        url_splited = task.url.split('/')
        if len(url_splited) > 1:
            data['page'] = url_splited[-1]
        if '_' not in data['page']:
            return
        for k, v in task.site.parse(grab, task.url):
            if len(v) == 1 and not isinstance(v[0], dict):
                v = ''.join(v)
            _v = '{}'.format(v)
            logger.info('{}: {}'.format(k, _v))
            data[k] = v
        if not data['description']:
            return
        self.dump_to_file(data, self.outputs[task.site.outfile])
        logger.info('---- COUNT {} ----'.format(self.ajax_counter))

    # def task_main(self, grab, task):
    #     logger.info('main {}'.format(task.url))
    #     champions_ships = grab.doc.select(task.site.link)
    #     for url in champions_ships:
    #         championship_url = '{}{}'.format(task.site.root_url, url.text())
    #         url_splited = url.text().split('/')
    #         match_id = ''
    #         yield Task('championship', url=championship_url, network_try_count=3, task_try_count=4, site=task.site)
    #         if len(url_splited) > 1:
    #             match_id = url_splited[-2]
    #         if not match_id:
    #             continue
    #         for tab in self.ajax_site.tabs:
    #             g = Grab()
    #             ajax_site = self.ajax_site
    #             ajax_url = self.ajax_site.root_url.format(99, match_id, tab)
    #             headers = ajax_site.headers
    #             headers['Referer'] = url.text()
    #             g.setup(headers=headers, url=ajax_url)
    #             yield Task('odds', grab=g, site=ajax_site, match_id=match_id, tab_id=tab)

    def task_odds(self, grab, task):
        # logger.info('grab {}, task {}'.format(grab, task))
        self.ajax_counter += 1
        logger.info('load info {}'.format(task.url))
        data = {'url': task.url, 'ajax_response': grab.doc.json['odds'], 'match_id': task.get('match_id'), 'tab_id': task.get('tab_id')}
        self.dump_to_file(data, self.outputs[self.ajax_site.outfile])
        logger.info('---- AJAX COUNT {} ----'.format(self.ajax_counter))

    def task_championship(self, grab, task):
        self.counter += 1
        logger.info('load info {}'.format(task.url))
        data = {'url': task.url}
        url_splited = task.url.split('/')
        if len(url_splited) > 1:
            data['id'] = url_splited[-2]
        for k, v in task.site.parse(grab, task.url):
            if len(v) == 1 and not isinstance(v[0], dict):
                v = ''.join(v)
            _v = '{}'.format(v)
            logger.info('{}: {}'.format(k, _v[:100]))
            data[k] = v
        self.dump_to_file(data, self.outputs[task.site.outfile])
        logger.info('---- COUNT {} ----'.format(self.counter))


if __name__ == '__main__':
    bot = BetSpider(thread_number=10)
    # bot.load_proxylist(source='proxy.txt', source_type='text_file', proxy_type='socks5')
    bot.run()
