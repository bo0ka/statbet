FROM gliderlabs/alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code

RUN set -x \
&& buildDeps='\
python-dev \
py-pip \
build-base \
' \
&& apk --update add python py-lxml $buildDeps \
&& apk --update add curl-dev python-dev libressl-dev \
&& rm -rf /var/cache/apk/* 
RUN apk add --update --no-cache g++ gcc libxslt-dev curl
RUN apk add --no-cache python-dev openssl-dev libffi-dev gcc && pip install --upgrade pip
RUN apk add chromium chromium-chromedriver
ENV INSTALL_PATH /code
WORKDIR $INSTALL_PATH

COPY requirements.txt ./
RUN pip install pydevd-pycharm~=192.7142.56
RUN pip install -r requirements.txt
COPY . .
RUN apk del --purge $buildDep
ADD . /code/