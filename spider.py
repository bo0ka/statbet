
class NetAPorterSpider(Spider):
    outputs = {}
    writes = {}
    counter = 0
    ajax_counter = 0

    def prepare(self):
        for site in sites:
            f = open(site.outfile, 'wb')
            f.write(b'[')
            self.outputs[site.outfile] = f
            self.writes[site.outfile] = False
        return super(NetAPorterSpider, self).prepare()

    def shutdown(self):
        for site in sites:
            if not site.outfile in self.outputs:
                continue
            f = self.outputs[site.outfile]
            if self.writes[site.outfile]:
                f.seek(-2, os.SEEK_END)
            f.write(b']')
            f.close()
        return super(NetAPorterSpider, self).shutdown()

    def dump_to_file(self, data, output):
        data = json.dumps(data, ensure_ascii=False)
        output.write(data.encode('utf8'))
        output.write(b',\n')

    def task_generator(self):
        g = Grab()
        for site in sites:
            s = site()
            yield Task('main', url=s.url, site=s, use_proxylist=False)

    def task_main(self, grab, task):
        logger.info('main {}'.format(task.url))
        if not hasattr(task, 'page_num'):
            setattr(task, 'page_num', '1')
        self.counter += 1
        site = task.site
        doc = grab.doc
        resp_list = doc.select(site.links).node_list()
        for r_url in sorted(resp_list):
            url = r_url.replace('\n', '')
            # if not '_' in url:
            #     continue
            yield Task('detail', url=''.join([site.root_url, url]), site=site, page_num=task.page_num)  #
        next_page = doc.select(site.next_page).node(default=None)
        logger.info('---- MAIN COUNT {} ----'.format(self.counter))
        if next_page or int(task.page_num) < 86:
            page_num = next_page and int(next_page.split('?pn=')[-1]) or (task.page_num + 1)
            yield Task('main', url='/'.join([site.root_url, 'ru/en/d/Shop', 'Clothing/All?pn=%s' % page_num]),
                       page_num=page_num,
                       site=site)
        else:
            logger.error('Cant find next page')

    def task_detail(self, grab, task):
        resp = grab.response
        if not resp.code == 200:
            logger.warning('{}'.format(resp.url))
            return
        self.ajax_counter += 1
        logger.info('load info {}'.format(task.url))
        data = {'url': task.url}
        url_splited = task.url.split('/')
        if len(url_splited) > 1:
            data['page'] = url_splited[-1]
        # if '_' not in data['page']:
        #     return
        for k, v in task.site.parse(grab, task.url):
            if len(v) == 1 and not isinstance(v[0], dict):
                v = ''.join(v)
            _v = '{}'.format(v)
            logger.info('{}: {}'.format(k, _v))
            data[k] = v
        # if not data['description']:
        #     return
        if 'images' in data and data['images']:
            images = data['images'].split(';')
            for i in images:
                part = i.split('.jpg')[0]
                src = ''.join(['http:', part[:-2], 'xl.jpg'])
                yield Task('image', url=src, product=data['reference'], filename=src.split('/')[-1])
        data['page_num'] = task.page_num
        self.dump_to_file(data, self.outputs[task.site.outfile])
        self.writes[task.site.outfile] = True
        logger.info('---- COUNT {} ----'.format(self.ajax_counter))

    def task_image(self, grab, task):
        path = 'results/images/%s/%s' % (task.product, task.filename)
        logger.info('For product {} image downloaded {} '.format(task.product, task.filename))
        grab.response.save(path)