## json dumps ##
* zoon - https://yadi.sk/d/JgRO-7Nu3KJAhW
* medbooking - https://yadi.sk/d/_uvAcEbr3KJKvY
* docdoc - https://yadi.sk/d/FTlIREka3KM4sQ

## clinics json dumps ##
* docdoc https://yadi.sk/d/qjtzDBIL3Kfv6e
* medbooking https://yadi.sk/d/RIW3EMFc3Kf5VB
* zoon https://yadi.sk/d/IULEct6R3L9bSE


# JSON Scheme #
* name - ФИО
* photo - фото
* info - Информация
* education - Образование
* specialty - Специальность
* specialization - Специализация
* experience - Стаж
* grade - Степень
* rating - Рейтинг
* reviews - отзывы (список)
    * author - От кого (всегда есть)
    * text - текст отзыва (всегда есть)
    * rank - оценка
    * date - дата
    * clinic - место приема
* price - стоимость первичного приема
    * для docdoc это список, который содержит словари
    * value (всегда есть)
    * discount
    * пример: ```[{'value': '1200 руб.', 'discount': '-10%'}]```
* services - услуги
* places - место приема (список)
    * clinic - клиника
    * address - адрес
    * metro - метро (список)
        * name - название метро
        * foot - пешая доступность
        * car - автомобильная доступность
* phone - телефон

# Requirements #

* python >= 3.5
* grablib


```
#!bash
virtualenv -p python3 .env
source .env/bin/activate
pip install -r requirements.txt
```

## Configure ##

**You need to create file conf.py**

Example:

```python
from parsers.docdoc import DocDocSite
from parsers.medbooking import MedBooking


sites = [MedBooking]
```


## How to run ##


```
#!bash

python main.py
```