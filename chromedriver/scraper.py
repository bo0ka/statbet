from selenium import webdriver
from datetime import timedelta, datetime
from time import sleep


class SpiderSelenium:
    path_chrome = '/opt/google/chrome/chrome'
    options = webdriver.ChromeOptions()
    current_url = ''
    last_connect = None

    def __init__(self):
        self.options.binary_location = self.path_chrome
        self.options.add_argument('headless')
        self.options.add_argument('disable-gpu')
        self.options.add_argument('no-sandbox')
        self.options.add_argument('window-size=1200x600')
        self.driver = webdriver.Chrome(chrome_options=self.options)

    def get_url(self, url):
        if not url == self.current_url:
            if self.last_connect:
                if self.last_connect >= datetime.now() - timedelta(seconds=5):
                    sleep(5)
                    self.last_connect = datetime.now()
            self.driver.get(url)
            self.current_url = url
            self.driver.set_page_load_timeout(10)

    def parse(self, xpath):
        return self.driver.find_elements_by_xpath(xpath)
