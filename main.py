# -*- coding: utf-8 -*-
import logging
import json
from grab import Grab
from grab.spider import Spider, Task
from grab.document import Document
from conf import sites
import os
from io import StringIO
import lxml
from converter import convert
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def update_data(func):
    def wrapper(spider, grab, task):
        resp = grab.response
        if not resp.code == 200:
            logger.warning('{}'.format(resp.url))
            return
        spider.ajax_counter += 1
        logger.info('load info {}'.format(task.url))
        data = {'url': task.url}
        for k, v in task.site.parse(grab, task.url):
            if len(v) == 1 and not isinstance(v[0], dict):
                v = ''.join(v)
            _v = '{}'.format(v)
            logger.info('{}: {}'.format(k, _v))
            data[k] = v
        task.data = data
        func(spider, grab, task)
    return wrapper


class NetAPorterSpider(Spider):
    outputs = {}
    writes = {}
    refferences = set()
    counter = 0
    ajax_counter = 0

    def prepare(self):
        for site in sites:
            f = open(site.outfile, 'wb')
            f.write(b'[')
            self.outputs[site.outfile] = f
            self.writes[site.outfile] = False
        return super(NetAPorterSpider, self).prepare()

    def shutdown(self):
        for site in sites:
            if not site.outfile in self.outputs:
                continue
            f = self.outputs[site.outfile]
            if self.writes[site.outfile]:
                f.seek(-2, os.SEEK_END)
            f.write(b']')
            f.close()
        return super(NetAPorterSpider, self).shutdown()

    def dump_to_file(self, data, output):
        data = json.dumps(data, ensure_ascii=False)
        output.write(data.encode('utf8'))
        output.write(b',\n')

    def task_generator(self):
        g = Grab()
        for site in sites:
            s = site()
            yield Task('main', url=s.url, site=s, use_proxylist=False)

    def task_main(self, grab, task):
        logger.info('main {}'.format(task.url))
        # if not hasattr(task, 'page_num'):
        #     setattr(task, 'page_num', '1')
        self.counter += 1
        site = task.site
        doc = grab.doc
        resp_list = doc.select(site.links).node_list()
        for r_url in sorted(resp_list):
            url = r_url.replace('\n', '')
            # if not '_' in url:
            #     continue
            yield Task('detail', url=''.join([site.root_url, url]), site=site)  #
        next_page = doc.select(site.next_page).node(default=None)

        logger.info('---- MAIN COUNT {} ----'.format(self.counter))
        if not next_page:
            logger.error('Cant find next page')
            return

        yield Task('main', url=site.url_next_page(site.next_page_func(next_page)), site=site)

    @update_data
    def task_detail(self, grab, task):
        # if not data['description']:
        #     return
        data = task.data
        if 'product_json' in data and data['product_json']:
            pj = json.loads(data['product_json'])['@graph'][0]
            if not data['color']:
                data['color'] = ';'.join(set([o['itemOffered']['color'] for o in pj['offers']['offers']]))
            if 'category' in pj:
                data['category'] = pj['category']
            if 'lowPrice' in pj['offers']:
                data['lowprice'] = pj['offers']['lowPrice']

        if 'images' in data and data['images']:
            images = data['images'].split(';')
            for i in images:
                image_url = i
                if not i.startswith('http'):
                    image_url = 'http:%s' % i
                yield Task('image', url=image_url, product=data['reference'], filename=i.split('/')[-1], site=task.site)
        if 'price' in data and data['price']:
            d = json.loads(data['price'])
            data['discount_percent'] = d.get('discountPersent', 0)
            data['price_original'] = d.get('originalAmount', 0)
            data['price_current'] = d.get('amount', 0)
            data['price_divisor'] = d.get('divisor',0)
            data['currency'] = d.get('currency', 0)

        data['page_num'] = self.counter
        if 'reference' in data and data['reference'] not in self.refferences:
            self.refferences.add(data['reference'])
        else:
            logger.warning(data)
        self.dump_to_file(data, self.outputs[task.site.outfile])
        self.writes[task.site.outfile] = True
        logger.info('---- COUNT {} ----'.format(self.ajax_counter))

    def task_image(self, grab, task):
        path = '%s/%s/%s' % (task.site.image_path, task.product, task.filename)
        logger.info('For product {} image downloaded {} '.format(task.product, task.filename))
        grab.response.save(path)


class NetAPorterDesignersSpider(NetAPorterSpider):

    def task_main(self, grab, task):
        logger.info('main {}'.format(task.url))
        self.counter += 1
        site = task.site
        doc = grab.doc
        resp_list = doc.select(site.links).node_list()
        for r_url in sorted(resp_list):
            url = r_url.replace('\n', '')
            yield Task('detail', url=''.join([site.root_url, url]), site=site)  #
        next_page = doc.select(site.next_page).node(default=None)

        logger.info('---- MAIN COUNT {} ----'.format(self.counter))

    @update_data
    def task_detail(self, grab, task):
        self.dump_to_file(task.data, self.outputs[task.site.outfile])
        self.writes[task.site.outfile] = True
        logger.info('---- COUNT {} ----'.format(self.ajax_counter))


if __name__ == '__main__':
    # bot = NetAPorterSpider(thread_number=10, task_try_limit=3, network_try_limit=3)
    bot = NetAPorterDesignersSpider(thread_number=10, task_try_limit=3, network_try_limit=3)
    # bot.load_proxylist(source='proxy.txt', source_type='text_file', proxy_type='socks5')
    bot.run()
