from .common import Site, parse, get_elements
import re


class MedbookingClinic(Site):
    root_url = 'http://medbooking.com'
    next_pattern = re.compile(r'\"(.*?)\"')
    url = 'http://medbooking.com/clinics'
    next_page_url = '//div[@class="paging"]//li[@class="paging__last"]/a/@href'
    link = '//div[@class="results results_clinic"]//div[@class="results__photo results_mob__photo"]/a/@href'
    review = {
        'author': './/div[@class="reviews__title"]',
        'text': './/div[@class="reviews__text"]',
        'rank': './/div[@class="reviews__overall"]'
    }
    place = {
        'address': './/meta[@itemprop="address"]',
        'metro': ('.//ul[@class="metro"]', {'name': './/li/span[@class="icon"]',
                                            'foot': './/li/span/span[@class="foot"]',
                                            'car': './/li/span/span[@class="car"]'
                                            })
    }
    outfile = 'medbooking_clinics.json'
    service_url = 'http://medbooking.com/services/page/{}?filters[clinic]={}'
    doctor_url = 'http://medbooking.com/doctors/page/{}?filters[clinic]={}'
    price = {
        'name': './/a[@class="services-item__title-link"]',
        'price': './/span[@class="services-item__price__value"]'
    }
    week = {
        'days': './/span[@class="hidden-sm hidden-xs"]',
        'hours': './/span[@class="card-item__week-date right"]'
    }

    def next_page(self, grab):
        href = grab.doc(self.next_page_url)
        if href:
            result = self.next_pattern.search(href.text())
            return result.group(1)

    @parse('//h1[@itemprop="name"]')
    def name(self, text):
        yield

    @parse('//div[@class="card-view__short-desc"]/div[@class="card-view__short-desc-text"]',
           '//div[@class="card-view__about-wrap clinic-text"]')
    def info(self, *data):
        for text in data:
            yield text

    @get_elements('//div[@class="card-view__card-info__photo-img"]/img/@src')
    def logo(self, img, grab):
        yield self.img_to_base64(img.text(), grab.clone())

    @get_elements('//div[@class="card-view__address js-cv-map-address"]')
    def address(self, element, grab):
        yield self.element_parse(self.place, element)

    @get_elements('//li[@class="card-item__week-li"]')
    def schedule(self, element, grab):
        yield self.element_parse(self.week, element)

    @get_elements('//div[contains(@class, "reviews__item reviews__item_new")]')
    def reviews(self, element, grab):
        yield self.element_parse(self.review, element)

    @get_elements('//input[@id="comment-clinic_id"]/@value')
    def doctors(self, element, grab):
        count = 1
        g = grab.clone()
        while True:
            url = self.doctor_url.format(count, element.text())
            g.go(url)
            if not g.doc.select('//div[@class="results__title results_mob__title"]/a'):
                raise StopIteration
            for name in g.doc.select('//div[@class="results__title results_mob__title"]/a'):
                yield name.text()
            count += 2

    @get_elements('//input[@id="comment-clinic_id"]/@value')
    def price_list(self, element, grab):
        count = 1
        g = grab.clone()
        while True:
            url = self.service_url.format(count, element.text())
            g.go(url)
            if not g.doc.select('//div[@class="services-item__wrapper"]'):
                raise StopIteration
            for price in g.doc.select('//div[@class="services-item__wrapper"]'):
                yield self.element_parse(self.price, price)
            count += 2
