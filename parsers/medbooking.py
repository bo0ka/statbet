from .common import Site, parse, get_elements
import re
import json


class MedBooking(Site):
    next_pattern = re.compile(r'\"(.*?)\"')
    root_url = 'http://medbooking.com'
    url = 'http://medbooking.com/doctors'
    next_page_url = '//div[@class="paging"]//li[@class="paging__last"]/a/@href'
    link = '//div[@class="results"]//div[@class="results__photo results_mob__photo"]/a/@href'
    review = {
        'author': './/div[@class="reviews__title"]',
        'text': './/div[@class="reviews__text"]',
        'rank': './/div[@class="reviews__overall"]'
    }

    place = {
        'clinic': './/meta[@itemprop="name"]/@content',
        'address': './/meta[@itemprop="address"]/@content',
        'metro': ('.//ul[@class="metro"]', {'name': './/li/span[@class="icon"]',
                                            'foot': './/li/span/span[@class="foot"]',
                                            'car': './/li/span/span[@class="car"]'
                                            })
    }
    outfile = 'medbooking.json'

    def next_page(self, grab):
        href = grab.doc(self.next_page_url)
        if href:
            result = self.next_pattern.search(href.text())
            return result.group(1)

    @parse('//h1')
    def name(self, text):
        yield

    @parse('//div[@class="card-view__about"]/div[@class="card-view__about-wrap"]/p')
    def info(self, text):
        yield

    @parse('//div[@class="card-view__about-title card-view__about-title_education"]/'
           'following-sibling::*[1][name()="div"]',
           '//div[@class="card-view__about-title card-view__about-title_high-education"]/'
           'following-sibling::*[1][name()="div"]'
           )
    def education(self, *text):
        yield

    @parse('//div[@class="card-view__direction"]/div')
    def specialty(self, text=''):
        yield

    @parse('//div[@class="card-view__direction"]/span')
    def experience(self, data=''):
        yield

    @parse('//div[@class="card-view__rating-info__average-value"]/span')
    def rating(self, data):
        yield

    @parse('//span[@class="card-view__primary-price-value"]')
    def price(self, text):
        yield

    @get_elements('//a[@class="order-btn order-btn_blue js-reviews-form-link hidden-sm hidden-xs"]/@data-eventparams')
    def phone(self, element, grab):
        doctor_id = element.text()
        g = grab.clone()
        headers = {"Referer": g.response.url}
        g.setup(headers=headers, post={'doctor_id': doctor_id, 'project': 'medbooking', 'phone': ''})
        g.go('http://outer.medbooking.com/real-phone-number/phone/')
        try:
            yield json.loads(g.response.body.decode('utf-8'))['phone']
        except ValueError:
            yield ''
        except KeyError:
            yield ''

    @get_elements('//div[@class="card-view__address js-cv-map-address"]')
    def places(self, element, grab):
        yield self.element_parse(self.place, element)

    @parse('//div[@class="card-view__services-list-item"]')
    def services(self, *args):
        for service in args:
            yield service.strip('\n')

    @get_elements('//div[contains(@class, "reviews__item reviews__item_new")]')
    def reviews(self, element, grab):
        yield self.element_parse(self.review, element)

    @get_elements('//div[@class="card-view__top"]/div[@class="card-view__photo"]/img/@src')
    def photo(self, img, grab):
        yield self.img_to_base64(img.text(), grab.clone())
