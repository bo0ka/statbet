# -*- coding: utf-8 -*-
from .common import Site, parse, get_elements
import re
import json


class GoldenLine(Site):
    root_url = 'http://golden-line.ru'
    page = 1

    _url = 'https://golden-line.ru/sitemap.xml'
    site_map_url = 'https://golden-line.ru/sitemap.xml'
    sitemap_links = '//urlset/url/loc/text()'
    allcollections = 'https://golden-line.ru/allcollections'
    links = '//*[@class="slides nomore"]/a/@href'
    next_page = '//*[@class="next"]/@href'
    odds_all = {
         'item': './/a[@class="list-tabs__item__in"]'
     }
    outfile = 'goldenline.json'

    @property
    def url(self):
        return self._url

    @parse('//*[@id="html_description"]')
    def description(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*/strong/a[@id="brand-link"]')
    def brand(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@id="product_info"]/div[1]/h1/span')
    def name(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@class="additional_info"]/span[1]')
    def color(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@class="additional_info"]/text()[2]')
    def material(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@class="additional_info"]/text()[3]')
    def country(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@class="additional_info"]/span[2]')
    def reference(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@id="product_info"]/div[@itemprop="offers"]/span[1]/text()')
    def price(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@id="product_info"]/div[@itemprop="offers"]/meta[@itemprop="availability"]/@content')
    def availability(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@class="additional_info"]')
    def full_info(self, text=''):
        yield text.replace('&nbsp;', ' ')


class GoldenLineTabs(Site):
    # GET params: p=<? get period>, e=<id>, b=<tab choice: 1x2, ou, ah, ha, dc, bts etc?>
    tabs = ['1x2', 'ou', 'ah', 'ha', 'dc', 'bts', ]
    root_url = 'http://www.betexplorer.com/gres/ajax/matchodds.php?p={}&e={}&b={}'
    headers = {
        'Accept': 'application/json',
        'User-Agent': 'X3',
    }
    link = '//table//td[1]/a/@href'
    row = '//table//tr'
    next_page_url = ''

    # review = {
    #     '': './/div[@class="comment-head"]/strong/span[@class="name"]',
    #     'text': './/div[@class="comment-container js-comment-container"]/'
    #             'div[@class="simple-text comment-text js-comment-text"]',
    # }
    #
    # place = {
    #     'clinic': './/div[@class="H3"]',
    #     'address': './/address'
    # }

    outfile = 'betexplorer_odds.json'

    @get_elements('//tr')
    def rows(self, element, grab):
        owner_id = element.text()
        yield {'oid': owner_id}
        # g = grab.clone()
        # sleep(ZoonClinic.TIMEOUT)
        # g.go(self.price_url.format(owner_id))
        # for element in g.doc.select('//li[@class="pricelist-item js-pricelist-item pull-left clearfix  hovered extended "]'):
        #     name = element.select('.//span[@class="js-pricelist-title"]/a')
        #     price = element.select('.//div[@class="price-weight fs-large clearfix"]/strong')
        #     if name and price:
        #         yield {'name': name.text(), 'price': price.text()}

    @parse('//table')
    def table(self, text=''):
        yield text