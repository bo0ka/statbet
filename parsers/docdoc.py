from .common import Site, parse, get_elements


class DocDocSite(Site):
    root_url = 'https://docdoc.ru'
    url = 'https://docdoc.ru/doctor'
    next_page_url = '//li/a[@class="paginate__item paginate__item_next"]/@href'
    link = '//div[@class="doctors__items"]//div[@class="doc"]//div[@class="doc__info"]/a[@class="doc__name"]/@href'
    review = {
        'author': './/div[@class="reviews__name"]/span[@class="review_author"]',
        'text': './/div[@class="reviews__text"]',
        'rating': './/div[@class="rate__value"]/@style',
        'date': './/span[@class="review_date"]',
        'clinic': './/div[@class="reviews__name"]/a'
    }
    place = {
        'clinic': './/meta[@itemprop="name"]/@content',
        'address': './/a[@itemprop="address"]',
        'metro': './/span[@class="rightbar__metro"]'
    }
    outfile = 'docdoc.json'

    @parse('//div[@class="doc__photo"]/span/img[@itemprop="image"]/@alt')
    def name(self, text):
        yield

    @parse('//div[@class="doctor__tab doctor__info"]/div[@class="doctor__text"]')
    def info(self, data):
        yield

    @parse('//div[@class="doctor__tab doctor__more"]/div/b[text()="Образование"]/following-sibling::*[1][name()="ul"]',
           '//div[@class="doctor__tab doctor__more"]/div/b[text()="Курсы повышения квалификации"]/following-sibling::'
           '*[1][name()="ul"]')
    def education(self, *args):
        yield

    @parse('//div[@class="doc__prof"]')
    def specialty(self, data):
        text = data.split('Стаж')[0]
        yield text

    @parse('//div[@class="doctor__tab doctor__more"]/div/b[text()="Специализация"]/following-sibling::div[ul]')
    def specialization(self, *args):
        for text in args:
            yield text
            raise StopIteration

    @parse('//div[@class="doc__prof"]')
    def experience(self, data):
        text = data.split('Стаж')[-1].split('/')[0]
        yield text

    @parse('//div[@class="doc__prof"]')
    def grade(self, data):
        if len(data.split('/')) > 1:
            text = data.split('/')[-1]
            yield text
        else:
            yield ''

    @parse('//div[@class="rate"]/div[@class="rate__value"]/@style')
    def rate(self, data):
        yield self.parse_rating(data)

    @get_elements('//div[@class="doc__price"]/span[contains(@class, "doc__price-value")]',
                  '//div[@class="doc__price"]/div[@class="doc__price-label"]')
    def price(self, price_value, price_discount='', grab=None):
        d = {'value': price_value.text()}
        if price_discount:
            d.update({'discount': price_discount.text()})
        yield d

    @get_elements('//div[@class="rightbar__place"]')
    def places(self, element, grab):
        yield self.element_parse(self.place, element)

    @get_elements('//div[@class="reviews__items"]/div[contains(@class, "reviews__item")]')
    def reviews(self, element, grab):
        yield self.element_parse(self.review, element)

    @get_elements('//div[@class="doc__photo"]/span/img[@itemprop="image"]/@src')
    def photo(self, img, grab):
        if 'data:' not in img.text():
            yield self.img_to_base64(img.text(), grab.clone())
