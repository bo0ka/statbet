# -*- coding: utf-8 -*-
from .common import Site, parse, parse_selenium, get_elements
import re
import json


class NeimanMarcusBase(Site):
    root_url = 'https://www.neimanmarcus.com'
    page = 1

    closing_unformated = '{}/en-ru/c/womens-clothing-clothing-cat58290731?navpath=cat000000_cat000001_cat58290731#endecaDrivenSiloRefinements=navpath%3Dcat000000_cat000001_cat58290731&personalizedPriorityProdId=000_cat000001_cat58290731&userConstrainedResults=true&refinements=&page={}&pageSize=120&sort=PCS_SORT&definitionPath=/nm/commerce/pagedef_rwd/template/EndecaDrivenHome&onlineOnly=&updateFilter=false&allStoresInput=false&rwd=true&catalogId=cat58290731&selectedRecentSize=&activeFavoriteSizesCount=0&activeInteraction=true'
    closing = closing_unformated.format(
        root_url, page)
    closing2 = '{}/c/womens-clothing-cat58290731?navpath=cat000000_cat000001_cat58290731&source=leftNav&page={}'.format(root_url, page)
    links = '//*[@id="productTemplateId"]/@href'
    next_page = '//a[@class="arrow-button--right"]/@href'
    next_page_num = '//*[@id="epagingTop"]/li[@class="pagingSlide active pagingNav next"]/@pagenum'
    outfile = '/media/booka/500/Net-A-Porter/neiman_marcus.json'
    image_path = '/media/booka/500/Net-A-Porter/marcus'

    @property
    def url(self):
        return self.closing

    @parse('//*[@class="productCutline"]/div/ul/li')
    def description(self, *descriptions):
        yield '|'.join(descriptions)

    @parse('(//div[@labeltxt]/@data-prodid)[1]')
    def reference(self, text=''):
        yield text.split('Online Inquiries: ')[-1]


class NeimanMarcus(NeimanMarcusBase):

    @parse('//h1/span[@class="product-designer"]/a/text()')
    def designer(self, text=''):
        yield text

    @parse('(//span[@class="prodDisplayName"]/text())[1]')
    def name(self, text=''):
        yield text

    @parse_selenium('//div[@labeltxt]/div/@data-value')
    def measures(self, *sizes):
        yield ';'.join(sizes)

    @parse('//div[contains(@name, "color")]/div/div/text()')
    def color(self, *colors):
        yield ';'.join(colors)

    @parse('//input[@name="retail0"]/@value')
    def price(self, text=''):
        yield text

    @parse('//ul[@class="list-inline"]//img/@data-zoom-url')
    def images(self, *links):
        yield ';'.join(links)

    @parse('//main/script[@type="application/ld+json"]')
    def product_json(self, text=''):
        yield text

    # @parse('//*[@id="main-product"]/meta/@content')
    # def category(self, text=''):
    #     yield text.replace('&nbsp;', ' ')
    #
    # @parse('//*[@id="main-product"]/div[@class="container-title"]/meta[@itemprop="url"]/@content')
    # def product_url(self, text=''):
    #     yield text
    #
    # @parse('//*[@id="accordion-2"]//li/text()')
    # def material(self, *material):
    #     yield ';'.join(material)
    #
    # @parse('//*[@id="accordion-2"]//p/a/@href')
    # def shown(self, *linked):
    #     yield ';'.join(['{}{}'.format(self.root_url, t) for t in linked])
    #
    # @parse('//*[@id="accordion-2"]//p/a/text()')
    # def shown_text(self, *linked_text):
    #     yield ';'.join(linked_text)
    #
    # @parse('//*[@id="accordion-1"]//li/text()')
    # def sizenfit(self, *text):
    #     yield ';'.join(text)