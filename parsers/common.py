import base64
import logging
from chromedriver.scraper import SpiderSelenium
selenium = SpiderSelenium()
logger = logging.getLogger(__name__)


def parse_selenium(*args):
    def decorator(func):
        def wrapper(self, grab):
            logger.info('search xpath {}'.format('\n'.join(args)))
            context = []
            selenium.get_url(self.current_url)
            for path in args:
                if not selenium.parse(path):
                    continue
                if len(selenium.parse(path)) > 1:
                    for text in selenium.parse(path):
                        context.append(text.text)
                else:
                    context.append(selenium.parse(path)[0].text)
            for result in func(self, *context):
                if result is None:
                    result = '\n'.join(context)
                    yield result
                    raise StopIteration
                else:
                    yield result.strip()
        wrapper.is_parse = True
        return wrapper
    return decorator


def parse(*args):
    def decorator(func):
        def wrapper(self, grab):
            logger.info('search xpath {}'.format('\n'.join(args)))
            context = []
            for path in args:
                if not grab.doc(path):
                    continue
                if grab.doc(path).count() > 1:
                    for text in grab.doc(path):
                        context.append(text.text())
                else:
                    context.append(grab.doc(path).text())
            for result in func(self, *context):
                if result is None:
                    result = '\n'.join(context)
                    yield result
                    raise StopIteration
                else:
                    yield result.strip()
        wrapper.is_parse = True
        return wrapper
    return decorator


def get_elements(*args):
    def decorator(func):
        def wrapper(self, grab):
            logger.info('search xpath {}'.format('\n'.join(args)))
            for path in args:
                if not grab.doc(path):
                    continue
                for element in grab.doc(path):
                    for result in func(self, element, grab=grab):
                        if result is None:
                            raise StopIteration
                        else:
                            yield result
        wrapper.is_parse = True
        return wrapper
    return decorator


def get_elements_selenium(*args):
    def decorator(func):
        def wrapper(self, grab):
            logger.info('search xpath {}'.format('\n'.join(args)))
            selenium.get_url(self.current_url)
            for path in args:
                if not selenium.parse(path):
                    continue
                for element in selenium.parse(path):
                    for result in func(self, element, grab=grab):
                        if result is None:
                            raise StopIteration
                        else:
                            yield result
        wrapper.is_parse = True
        return wrapper
    return decorator


class Site(object):
    url = ''
    link = ''
    fields = {}
    next_page_url = ''
    root_url = ''
    current_url = ''
    TIMEOUT = 1

    def __init__(self):
        for field in dir(self):
            if getattr(getattr(self, field), 'is_parse', False):
                self.fields[field] = getattr(self, field)

    def parse(self, grab, url):
        self.current_url = url
        for key, method in self.fields.items():
            yield key, list(method(grab))

    def next_page(self, grab):
        result = grab.doc(self.next_page_url)
        if result:
            return result.text()

    def parse_rating(self, raw_rating):
        percent = raw_rating.split(':')[-1]
        percent = int(percent.replace('%', '').replace(';', '')) / 100
        rating = percent * 5
        return str(rating)[:3]

    def element_parse(self, mapping, element_grab):
        result = {}
        for k, v in mapping.items():
            if isinstance(v, tuple):
                item = []
                for element in element_grab.select(v[0]):
                    item.append(self.element_parse(v[1], element))
            elif not element_grab.select(v):
                item = ''
            elif k == 'rating':
                item = self.parse_rating(element_grab.select(v).text())
                item = item.strip()
            else:
                item = element_grab.select(v).text()
                item = item.strip(',').strip()
            result[k] = item
        return result

    def element_parse_selenium(self, mapping, element_selenium):
        result = {}
        for k, v in mapping.items():
            if isinstance(v, tuple):
                item = []
                for element in element_selenium.find_elements_by_xpath(v[0]):
                    item.append(self.element_parse_selenium(v[1], element))
            elif not element_selenium.find_element_by_xpath(v):
                item = ''
            elif k == 'rating':
                item = self.parse_rating(element_selenium.find_element_by_xpath(v).text)
                item = item.strip()
            else:
                item = element_selenium.find_element_by_xpath(v).text
                item = item.strip(',').strip()
            result[k] = item
        return result

    def img_to_base64(self, url, grab):
        folder = 'images'
        # url = '{}{}'.format(self.root_url, url)
        grab.go(url, timeout=100)
        img = grab.doc.save_hash(url, folder, ext="jpg")
        with open('{}/{}'.format(folder, img), "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
            return encoded_string.decode()
