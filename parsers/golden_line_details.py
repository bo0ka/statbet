from .common import Site, parse, get_elements, parse_selenium, get_elements_selenium
from time import sleep


class BetExplorerBookmakers(Site):
    root_url = ''
    # page = 1
    # _url = 'https://zoon.ru/msk/medical/?action=list&type=service&page={}&need[]=items'
    # next_page_url = '//span[@class="js-next-page button button40 button-primary button-block"]'
    # link = '//div[@class="service-description"]/div[@class="H3"]/a/@href'
    # review = {
    #     'author': './/div[@class="comment-head"]/strong/span[@class="name"]',
    #     'text': './/div[@class="comment-container js-comment-container"]/'
    #             'div[@class="simple-text comment-text js-comment-text"]',
    # }
    #
    # specialization = {
    #     'root': './/dt',
    #     'categories': './/dd'
    # }
    #
    # place = {
    #     'address': './/address',
    #     'metro': './/div[strong[text()="Метро"]]/span'
    # }
    outfile = 'goldenline_details.json'
    # price_url = 'https://zoon.ru/js.php?area=menu&action=itemList&owner_type=organization&' \
    #             'owner_id={}&category=&search=&page='
    #
    # review_url = 'https://zoon.ru/js.php?area=service&action=CommentList&owner[]=' \
    #              'organization&owner[]=prof&organization={}&' \
    #              'seo_target_type=organization&is_widget=0&snippet=&sort=default&limit=10000&skip=0&strategy=All'
    TIMEOUT = 5

    @property
    def url(self):
        return self._url.format(self.page)

    def next_page(self, grab):
        next_button = grab.doc(self.next_page_url)
        if next_button:
            self.page += 1
            return self._url.format(self.page)

    @parse('//h1/span[@itemprop="name"]')
    def name(self, text):
        yield

    @parse('//div[@class="service-about"]//dl/dd[@class="simple-text js-desc oh word-break"]')
    def info(self, *data):
        for text in data:
            yield text
            raise StopIteration

    @get_elements('//div[@class="service-box-description box-padding"]//dl/dd[address]')
    def address(self, element, grab):
        yield self.element_parse(self.place, element)

    @parse_selenium('//dd[@class="simple-text"]/div[strong[text()="Время работы"]]')
    def schedule(self, text=''):
        yield

    @parse_selenium('//dl[@class="fluid" and dt[text()="Тип мед. учреждения"]]/dd')
    def type_company(self, text=''):
        yield

    @get_elements_selenium('//dl[@class="fluid" and dt[text()="Тип мед. учреждения"]]/following-sibling::dl')
    def specializations(self, element, grab):
        yield self.element_parse_selenium(self.specialization, element)

    @get_elements('//ul[@class="list-reset prof-list js-results-list js-results-container"]')
    def doctors(self, element, grab):
        g = grab.clone()
        count = 1
        while True:
            sleep(BetExplorerChampionships.TIMEOUT)
            g.go('?action=specialists&mode=zoon&first_page=1&page={}'.format(count))
            element = g.doc.select('//li//div[@class="prof-name H3"]/a')
            if not element:
                break
            for name in element:
                yield name.text()
            count += 1

    @get_elements('//div[@id="reviews"]/div/@data-owner-id')
    def price_list(self, element, grab):
        owner_id = element.text()
        g = grab.clone()
        sleep(BetExplorerChampionships.TIMEOUT)
        g.go(self.price_url.format(owner_id))
        for element in g.doc.select('//li[@class="pricelist-item js-pricelist-item pull-left clearfix  hovered extended "]'):
            name = element.select('.//span[@class="js-pricelist-title"]/a')
            price = element.select('.//div[@class="price-weight fs-large clearfix"]/strong')
            if name and price:
                yield {'name': name.text(), 'price': price.text()}

    @get_elements('//div[@id="reviews"]/div/@data-owner-id')
    def reviews(self, element, grab):
        g = grab.clone()
        sleep(BetExplorerChampionships.TIMEOUT)
        g.go(self.review_url.format(element.text()))
        if 'list' not in g.doc.json:
            raise StopIteration
        g.doc.body = g.doc.json['list'].encode()
        for review in g.doc.select('//li'):
            yield self.element_parse(self.review, review)

    @get_elements('//ul[@class="gallery-nav"]/li/a/@data-original')
    def images(self, element, grab):
        # sleep(ZoonClinic.TIMEOUT)
        yield self.img_to_base64(element.text(), grab.clone())
