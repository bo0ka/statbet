from .common import Site, parse, get_elements
import re
import json


class BetExplorer(Site):
    root_url = 'http://golden-line.ru'
    page = 1

    _url = 'https://golden-line.ru/sitemap.xml'
    link = '//table//td[1]/a/@href'
    site_map_url = 'http://betexplorer.com/site-map.php'
    links = '//div/section/ul[1]/li[2]/ul/li[1]/ul/li/ul/li/a/@href'
    next_page_url = ''
    odds_all = {
         'item': './/a[@class="list-tabs__item__in"]'
     }
    outfile = 'betexplorer.json'

    @property
    def url(self):
        return self._url

    @parse('//h1')
    def cup(self, text=''):
        yield text.replace('&nbsp;', ' ')


    @parse('//title')
    def event_dt(self, text=''):
        mobj = re.search('.+, (\d+\.\d+\.\d+) .+', text)
        mstr = text
        if mobj:
            mstr = mobj.group(1)
        yield mstr

    @parse('//*[@id="js-score"]')
    def total_score(self, text=''):
        yield text

    @parse('//*[@id="js-partial"]')
    def paritial_score(self, text=''):
        yield text

    @parse('//*[@id="js-eventstage"]')
    def stage(self, text=''):
        yield text

    @parse('//li[1]/*[@class="list-details__item__title"]/a')
    def team1(self, text=''):
        yield text

    @parse('//li[3]/*[@class="list-details__item__title"]/a')
    def team2(self, text=''):
        yield text

    @parse('//ul[@class="list-breadcrumb"]/li[2]/a')
    def sport_kind(self,text=''):
        yield text

    @parse('//ul[@class="list-breadcrumb"]/li[3]/a')
    def country(self, text=''):
        yield text

    @parse('//ul[@class="list-breadcrumb"]/li[5]/span')
    def teams(self, text=''):
        yield text

    @get_elements('//*[@id="odds-all"]//ul[@class="list-tabs"]')
    def odds(self, element, grab):
         yield self.element_parse(self.odds_all, element)
    #
    # @get_elements('//*[@id="odds-content"]')
    # def over_under(self, element, grab):
    #     yield self.element_parse(self.review, element)
    #
    # @get_elements('//*[@id="odds-content"]')
    # def asian_handicap(self, element, grab):
    #     yield self.element_parse(self.review, element)
    #
    # @get_elements('//*[@id="odds-content"]')
    # def draw_not_bet(self, element, grab):
    #     yield self.element_parse(self.review, element)
    #
    # @get_elements('//*[@id="odds-content"]')
    # def double_chance(self, element, grab):
    #     yield self.element_parse(self.review, element)


class BetExploterTabs(Site):
    # GET params: p=<? get period>, e=<id>, b=<tab choice: 1x2, ou, ah, ha, dc, bts etc?>
    tabs = ['1x2', 'ou', 'ah', 'ha', 'dc', 'bts', ]
    root_url = 'http://www.betexplorer.com/gres/ajax/matchodds.php?p={}&e={}&b={}'
    headers = {
        'Accept': 'application/json',
        'User-Agent': 'X3',
    }
    link = '//table//td[1]/a/@href'
    row = '//table//tr'
    next_page_url = ''

    # review = {
    #     '': './/div[@class="comment-head"]/strong/span[@class="name"]',
    #     'text': './/div[@class="comment-container js-comment-container"]/'
    #             'div[@class="simple-text comment-text js-comment-text"]',
    # }
    #
    # place = {
    #     'clinic': './/div[@class="H3"]',
    #     'address': './/address'
    # }

    outfile = 'betexplorer_odds.json'

    @get_elements('//tr')
    def rows(self, element, grab):
        owner_id = element.text()
        yield {'oid': owner_id}
        # g = grab.clone()
        # sleep(ZoonClinic.TIMEOUT)
        # g.go(self.price_url.format(owner_id))
        # for element in g.doc.select('//li[@class="pricelist-item js-pricelist-item pull-left clearfix  hovered extended "]'):
        #     name = element.select('.//span[@class="js-pricelist-title"]/a')
        #     price = element.select('.//div[@class="price-weight fs-large clearfix"]/strong')
        #     if name and price:
        #         yield {'name': name.text(), 'price': price.text()}

    @parse('//table')
    def table(self, text=''):
        yield text