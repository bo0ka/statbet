from .common import Site, parse, get_elements
import re
import json


class Zoon(Site):
    root_url = ''
    page = 1
    _url = 'https://zoon.ru/msk/p-doctor/?action=list&page={}'
    next_page_url = '//span[@class="js-next-page button button40 button-primary button-block"]'
    link = '//div[@class="prof-about prof-offset-photo"]/div[@class="prof-name H3"]/a/@href'
    review = {
        'author': './/div[@class="comment-head"]/strong/span[@class="name"]',
        'text': './/div[@class="comment-container js-comment-container"]/'
                'div[@class="simple-text comment-text js-comment-text"]',
    }

    place = {
        'clinic': './/div[@class="H3"]',
        'address': './/address'
    }
    outfile = 'zoon.json'

    @property
    def url(self):
        return self._url.format(self.page)

    def next_page(self, grab):
        next_button = grab.doc(self.next_page_url)
        if next_button:
            self.page += 1
            return self._url.format(self.page)

    @parse('//h1[@itemprop="name"]')
    def name(self, text):
        yield text.replace('&nbsp;', ' ')

    @parse('//dl[@class="fluid"]/dt[text()="Информация"]/following-sibling::*[1][name()="dd"]')
    def info(self, text):
        yield

    @parse('//dl[@class="fluid"]/dt[text()="Образование"]/following-sibling::*[1][name()="dd"]')
    def education(self, *text):
        yield

    @parse('//dl[@class="fluid"]/dt[text()="Специальность"]/following-sibling::*[1][name()="dd"]')
    def specialty(self, text):
        yield

    @parse('//dl[@class="fluid"]/dt[text()="Специализация"]/following-sibling::*[1][name()="dd"]')
    def specialization(self, text=''):
        yield

    @parse('//div[@class="prof-stars"]/div/div[@class="stars-view stars-view-big clearfix"]/@data-val')
    def rating(self, data):
        yield

    @parse('//meta[@itemprop="priceRange"]/@content')
    def price(self, text=''):
        yield text.replace('от', '').replace('руб', '')

    @parse('//div[@class="prof-about prof-offset-photo"]//'
           'a[@class="button button-silver button-static js-phone-number"]/@href')
    def phone(self, text=''):
        yield text.replace('tel:', '')

    @get_elements('//ul[@id="ProfAddresses"]/li//div[@class="service-description"]')
    def places(self, element, grab):
        yield self.element_parse(self.place, element)

    @parse('//div[@class="pricelist-group btop pd-m"]/ul/li')
    def services(self, *args):
        for service in args:
            yield service.replace('&nbsp;', '')

    @get_elements('//ul[@class="list-reset feedbacks clearfix js-comment-list"]/li[@class="js-comment"]')
    def reviews(self, element, grab):
        yield self.element_parse(self.review, element)

    @get_elements('//div[@itemtype="http://schema.org/Physician"]/meta[@itemprop="image"]/@content')
    def photo(self, img, grab):
        yield self.img_to_base64(img.text(), grab.clone())
