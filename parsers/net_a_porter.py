# -*- coding: utf-8 -*-
from .common import Site, parse, get_elements
import re
import json
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class NetAPorterDesignersBase(Site):
    root_url = 'http://net-a-porter.com'
    raw_section = '/Shop/AZDesigners'
    closing = '{}{}'.format(root_url, raw_section)

    links = '//*[@class="designer_list_col"]//a/@href'
    outfile = 'results/net-a-porter_designers.json'

    @parse('//*[@id="desc"]/h1')
    def brand(self, text=''):
        yield text

    @parse('//*[@id="desc"]/p')
    def seo_text(self, text=''):
        yield text

    @property
    def url(self):
        return self.closing


class NetAPorterBase(Site):
    root_url = 'http://net-a-porter.com'
    page = 1

    # closing = 'https://net-a-porter.com/ru/en/d/Shop/Clothing/All?pn={}'.format(page)
    data = {
        'section': {}
            }
    section = ''
    section_url = ''
    if section:
        section_url = 'Sale/'

    raw_section = '/ru/en/d/Shop/{section}Clothing/All?pn='.format(section=section_url)
    closing = '{}{}{}'.format(root_url, raw_section, page)

    links = '//*[@class="product-image"]/a/@href'
    next_page = '//*[@class="next-page"]/@href'
    outfile = 'results/net-a-porter_{section}.json'.format(section=section)
    image_path = '/media/booka/500/Net-A-Porter/results'

    @staticmethod
    def next_page_func(url):
        return int(url.split('?pn=')[-1])

    def url_next_page(self, page):
        return '{root}{raw}{page}'.format(root=self.root_url, raw=self.raw_section, page=page)

    @property
    def url(self):
        return self.closing

    @parse('//*[@id="accordion-2"]//p')
    def description(self, *descriptions):
        descriptions = descriptions[0].split('Shown here')
        yield descriptions[0]

    @parse('//*[@id="main-product"]/div[@class="container-title"]/meta[@itemprop="productID"]/@content')
    def reference(self, text=''):
        yield text


class NetAPorter(NetAPorterBase):

    @parse('//*[@id="main-product"]/meta/@content')
    def category(self, text=''):
        yield text.replace('&nbsp;', ' ')

    @parse('//*[@id="main-product"]/div[@class="container-title"]/meta[@itemprop="name"]/@content')
    def name(self, text=''):
        yield text

    @parse('//*[@class="designer-name"]/span/text()')
    def designer(self, text=''):
        yield text

    @parse('//*[@id="main-product"]/div[@class="container-title"]/meta[@itemprop="url"]/@content')
    def product_url(self, text=''):
        yield text

    @parse('//*[@id="main-product"]//nap-price/@price')
    def price(self, text=''):
        yield text

    @parse('//*[@id="main-product"]//ul/li/img[@class="thumbnail-image"]/@src')
    def images(self, *links):
        yield ';'.join(links)

    @parse('//*[@id="product-video"]/source/@src')
    def video(self, text=''):
        yield text

    @parse('//*[@class="sku"]/@options')
    def measures(self, *datasizes):
        text = datasizes[0]
        if len(datasizes) != 1:
            logger.warning('{}'.format('|'.join(datasizes)))
        result = [m['data']['size'] for m in json.loads(text)]
        yield ';'.join(result)

    @parse('//nap-product-swatch-collector/@pids')
    def pids(self, text=''):
        yield text

    @parse('//*[@id="accordion-2"]//li/text()')
    def material(self, *material):
        yield ';'.join(material)

    @parse('//*[@id="accordion-2"]//p/a/@href')
    def shown(self, *linked):
        yield ';'.join(['{}{}'.format(self.root_url, t) for t in linked])

    @parse('//*[@id="accordion-2"]//p/a/text()')
    def shown_text(self, *linked_text):
        yield ';'.join(linked_text)

    @parse('//*[@id="accordion-1"]//li/text()')
    def sizenfit(self, *text):
        yield ';'.join(text)
