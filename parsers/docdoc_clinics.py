from .common import Site, parse, get_elements


class DocDocClinic(Site):
    root_url = 'https://docdoc.ru'
    url = 'https://docdoc.ru/clinic/page/1'
    next_page_url = '//li/a[@class="paginate__item paginate__item_next"]/@href'
    link = '//div[@class="clinics-items"]//div[@class="clinic-card__info"]/a[@class="clinic-card__name"]/@href'
    review = {
        'author': './/div[@class="reviews__name"]/span[@itemprop="author"]',
        'text': './/div[@class="reviews__text"]',
        'rating': './/div[@class="rate__value"]/@style',
        'date': './/span[@itemprop="datePublished"]/@content',
    }
    place = {
        'address': './/span[@itemprop="streetAddress"]',
        'metro': ('.//div[@class="clinic__metro"]', {'name': './/a'})
    }

    review_url = 'https://docdoc.ru/clinic/moreReviews?clinicId={}&speciality=&isBranchGroup=1&offset={}'
    doctor_url = 'https://docdoc.ru/clinic/moreDoctors?clinicId={}&speciality=&isBranchGroup=1&offset={}'
    price = {
        'name': './/td[@class="price-table__dot"]',
        'price': './/td[@class="price-table__dot price-table__cost"]'
    }

    outfile = 'docdoc_clinics.json'
    page = 0

    def next_page(self, grab):
        if self.page < 38:
            self.page += 1
            return '/clinic/page/{}'.format(self.page)

    @parse('//h1[@class="clinic__name"]')
    def name(self, text):
        yield

    @parse('//div[@class="clinic__text js-short-description"]/div[@itemprop="description"]')
    def info(self, data=''):
        yield

    @get_elements('//div[@class="clinic__logo "]/img/@src')
    def logo(self, img, grab):
        yield self.img_to_base64(img.text(), grab.clone())

    @parse('//h2[text()="Специализация"]/'
           'following-sibling::div[@class="clinic__links-inner"]')
    def specialization(self, *args):
        for text in args:
            yield text

    @get_elements('//div[@class="clinic__info"]')
    def address(self, element, grab):
        yield self.element_parse(self.place, element)

    @get_elements('//div[@class="js-map-teaser hidden"]/@data-clinic-id')
    def reviews(self, element, grab):
        g = grab.clone()
        count = 0
        clinic_id = element.text()
        while True:
            g.go(self.review_url.format(clinic_id, count))
            reviews = g.doc.json['reviews']
            if not reviews:
                raise StopIteration
            reviews = b''.join(map(str.encode, reviews))
            g.doc.body = reviews
            for review in g.doc.select('//div[@itemprop="review"]'):
                yield self.element_parse(self.review, review)
                print(self.element_parse(self.review, review))
            count += 20

    @get_elements('//div[@class="js-map-teaser hidden"]/@data-clinic-id')
    def doctors(self, element, grab):
        g = grab.clone()
        count = 0
        clinic_id = element.text()
        while True:
            g.go(self.doctor_url.format(clinic_id, count))
            doctors = g.doc.json['doctors']
            if not doctors:
                raise StopIteration
            doctors = b''.join(map(str.encode, doctors))
            g.doc.body = doctors
            for doctor in g.doc.select('//a[@class="doc__name"]'):
                yield doctor.text()
            count += 10

    @get_elements('//table[@class="price-table expanded"]')
    def price_list(self, element, grab):
        yield self.element_parse(self.price, element)

    @parse('//div[@class="clinic__worktime-row"]')
    def schedule(self, *args):
        for text in args:
            yield text
