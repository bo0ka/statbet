# -*- coding: utf-8 -*-
import csv
import json
import pandas as pd
from collections import deque
from conf import sites


def convert_site(x):
    f = csv.writer(open("%s.csv" % filename, "w+"))
    first = True
    d = deque()
    for item in x:
        if first:
            first = False
            for k, v in item.items():
                d.append(k)
            f.writerow([h for h in d])

        f.writerow(';'.join(['"%s"' % item[h] for h in d]))


def convert():
    for site in sites:
        filename = site.outfile.split('.json')[0]

        df = pd.read_json('%s.json' % filename)
        csv = df.to_csv('%s.csv' % filename, sep="\t", encoding='utf-8')


if __name__ == '__main__':
    convert()




