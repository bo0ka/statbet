## Ansible ##

### You should run ssh-agent

```
eval `ssh-agent`
ssh-add
```


### Initial setup ###

```
ansible-playbook -i hosts setup.yml -v
```

It will install required packages and chrome 57
Repository will be in folder /usr/src/medparser
It will install python requirements as well


### Update ###
```
ansible-playbook -i hosts update.yml -v
```

It will update repository
Repository will be in folder /usr/src/medparser
